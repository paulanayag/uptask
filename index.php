<?php
include 'includes/functions/sesiones.php';
include 'includes/functions/funciones.php';
include 'includes/functions/conexion-bbdd.php';
include 'includes/templates/header.php';
include 'includes/templates/barra.php';

$id_proyecto = isset($_GET['id_proyecto']) ? $_GET['id_proyecto'] : null;
?>
<div class="contenedor">
    <?php include 'includes/templates/sidebar.php'; ?>
    <main class="contenido-principal">
        <?php
        $proyecto = obtenerNombreProyecto($id_proyecto);
        if ($proyecto) : ?>
            <h1>
                <?php foreach ($proyecto as $nombre): ?>
                    <span><?php echo $nombre['nombre_proyecto']; ?></span>
                <?php endforeach; ?>
            </h1>
            <form action="#" class="agregar-tarea">
                <div class="campo">
                    <label for="tarea">Tarea:</label>
                    <input type="text" placeholder="Nombre Tarea" class="nombre-tarea">
                </div>
                <div class="campo enviar">
                    <input type="hidden" id="id_proyecto" value="<?php echo $_GET['id_proyecto']; ?>">
                    <input type="submit" class="boton nueva-tarea" value="Agregar">
                </div>
            </form>
        <h2>Listado de tareas:</h2>
        <div class="listado-pendientes">
            <ul>
                <?php
                $tareas = obtenerTareas($id_proyecto);
                if ($tareas->num_rows > 0) { ?>
                    <?php foreach ($tareas as $tarea): ?>
                        <li id="tarea: <?php echo $tarea['id'] ?>" class="tarea">
                            <p><?php echo $tarea['nombre_tarea']; ?></p>
                            <div class="acciones">
                                <i class="far fa-check-circle <?php echo ($tarea['estado'] === '1' ? 'completo' : ''); ?>"></i>
                                <i class="fas fa-trash"></i>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php } else {
                    echo "<p class='lista-vacia'>No hay tareas en este proyecto</p>";
                }
                ?>
            </ul>
        </div>
        <div class="avance">
            <h2>Avance del proyecto:</h2>
            <div id="barra-avance" class="barra-avance">
                <div id="porcentaje" class="porcentaje">
                </div>
            </div>
        </div>
        <?php else: ?>
            <div class="bienvenido">
                <h2>¡BIENVENIDO/A AL ADMINISTRADOR DE PROYECTOS!</h2>
                <p>En el menú izquierdo puedes crear nuevoS proyectos y seleccionar los que tengas ya creados</p>
                <p>En cada proyecto podrás llevar un registro de las tareas</p>
                <p>Crea nuevas tareas, elimina las que ya no te hagan falta y actualiza las que hayas finalizado</p>
                <p>Podrás llevar un registro de tu progreso con una barra de porcentaje</p>
            </div>
        <?php endif; ?>
    </main>
</div>
<!--.contenedor-->
<?php include 'includes/templates/footer.php'; ?>