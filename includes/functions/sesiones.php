<?php
    function usuario_autenticado() {
        if(!check_usuario()) {
            header('Location:login.php');
            exit();
        }
    }
    function check_usuario() {
        return isset($_SESSION['nombre']);
    }
    session_start();
    usuario_autenticado();
?>