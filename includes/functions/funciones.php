<?php

function obtenerPaginaActual() {
    $archivo = basename($_SERVER['PHP_SELF']);
    $pagina = str_replace(".php", "", $archivo);
    return $pagina;
}

function obtenerProyectos() {
    include 'conexion-bbdd.php';
    try {
        return $connection->query("SELECT id, nombre_proyecto FROM proyectos");
    } catch(Exception $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}

function obtenerNombreProyecto($id = null) {
    include 'conexion-bbdd.php';
    try {
        return $connection->query("SELECT nombre_proyecto FROM proyectos WHERE id = {$id}");
    } catch(Exception $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}

function obtenerTareas($id = null) {
    include 'conexion-bbdd.php';
    try {
        return $connection->query("SELECT id, nombre_tarea, estado FROM tareas WHERE id_proyecto = {$id}");
    } catch(Exception $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}

?>