<?php
    include '../functions/conexion-bbdd.php';
    $accion = $_POST['accion'] ? $_POST['accion'] : null;

    if($accion === 'crear') {
        $tarea = $_POST['tarea'] ?  $_POST['tarea'] : null;
        $id_proyecto = $_POST['id_proyecto'] ? (int) $_POST['id_proyecto'] : null;
        try {
            // Consulta BBDD
            $stmt = $connection->prepare("INSERT INTO tareas (nombre_tarea, id_proyecto) VALUES (?, ?)");
            $stmt->bind_param('si', $tarea, $id_proyecto);
            $stmt->execute();
            if($stmt->affected_rows > 0) {
                $respuesta = array(
                    'respuesta' => 'ok',
                    'id_tarea' => $stmt->insert_id,
                    'accion' => $accion,
                    'tarea' => $tarea
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko',
                    'accion' => $accion
                );
            }
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

    if($accion === 'actualizar') {
        $id_tarea = $_POST['id_tarea'] ? (int) $_POST['id_tarea'] : null;
        $estado = $_POST['estado'] ? $_POST['estado'] : null;
        try {
            $stmt = $connection->prepare("UPDATE tareas set estado = ? WHERE id = ?");
            $stmt->bind_param('ii', $estado, $id_tarea);
            $stmt->execute();
            if($stmt->affected_rows > 0) {
                $respuesta = array(
                    'respuesta' => 'ok'
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko',
                    'accion' => $accion
                );
            }
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

    if($accion === 'eliminar') {
        $id_tarea = $_POST['id_tarea'] ? (int) $_POST['id_tarea'] : null;
        try {
            $stmt = $connection->prepare("DELETE FROM tareas WHERE id = ?");
            $stmt->bind_param('i', $id_tarea);
            $stmt->execute();
            if($stmt->affected_rows > 0) {
                $respuesta = array(
                    'respuesta' => 'ok'
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko',
                    'accion' => $accion
                );
            }
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }
?>