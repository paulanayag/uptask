<?php
    include '../functions/conexion-bbdd.php';
    $accion = $_POST['accion'] ? $_POST['accion'] : null;
    $proyecto = $_POST['proyecto'] ? $_POST['proyecto'] : null;

    if($accion === 'crear') {
        try {
            // Consulta BBDD
            $stmt = $connection->prepare("INSERT INTO proyectos (nombre_proyecto) VALUES (?)");
            $stmt->bind_param('s', $proyecto);
            $stmt->execute();
            if($stmt->affected_rows > 0) {
                $respuesta = array(
                    'respuesta' => 'ok',
                    'id' => $stmt->insert_id,
                    'accion' => $accion,
                    'nombre' => $proyecto
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko',
                    'accion' => $accion
                );
            }
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }
?>