<?php
    include '../functions/conexion-bbdd.php';
    $accion = $_POST['accion'] ? $_POST['accion'] : null;
    $usuario = $_POST['usuario'] ? $_POST['usuario'] : null;
    $password = $_POST['password'] ? $_POST['password'] : null;

    // Crear usuario
    if($accion === 'crear') {
        $opciones = array(
            'cost' => 12
        );
        $hash_password = password_hash($password, PASSWORD_BCRYPT, $opciones);
        try {
            // Consulta BBDD
            $stmt = $connection->prepare("INSERT INTO usuarios (usuario, password) VALUES (?, ?)");
            $stmt->bind_param('ss', $usuario, $hash_password);
            $stmt->execute();
            if($stmt->affected_rows > 0) {
                $respuesta = array(
                    'respuesta' => 'ok',
                    'id_insertado' => $stmt->insert_id,
                    'tipo' => $accion
                );
            } else {
                $respuesta = array(
                    'respuesta' => 'ko',
                    'tipo' => $accion
                );
            }
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

    // Login
    if($accion === 'login') {
        try {
            // Consulta BBDD
            $stmt = $connection->prepare("SELECT id, usuario, password FROM usuarios WHERE usuario = ?");
            $stmt->bind_param('s', $usuario);
            $stmt->execute();
            $stmt->bind_result($id_usuario, $nombre_usuario, $pass_usuario);
            $stmt->fetch();

            if($nombre_usuario) {
                if(password_verify($password, $pass_usuario)) {
                    // ok login
                    session_start();
                    $_SESSION['nombre'] = $nombre_usuario;
                    $_SESSION['id'] = $id_usuario;
                    $respuesta = array(
                        'respuesta' => 'ok',
                        'tipo' => $accion,
                        'nombre' => $nombre_usuario
                    );
                } else {
                    // ko login
                    $respuesta = array(
                        'respuesta' => 'ko',
                        'error' => 'password incorrecto',
                        'tipo' => $accion
                    );
                }
            } else {
                $respuesta = array(
                    'error' => 'usuario no existe',
                    'tipo' => $accion
                );
            }
          
            $stmt->close();
            $connection->close();
        } catch(Exception $e) {
            $respuesta = array(
                'error' => $e->getMessage()
            );
        }
        echo json_encode($respuesta);
    }

?>