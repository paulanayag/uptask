eventListeners();

function eventListeners() {
    if(document.querySelector('#formulario')) {
        document.querySelector('#formulario').addEventListener('submit', validarRegistro);
    }   
}

function validarRegistro(e) {
    e.preventDefault();
    var usuario = document.querySelector('#usuario').value;
    var password = document.querySelector('#password').value;
    var tipo = document.querySelector('#tipo').value;

    if(!usuario || !password) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Todos los campos son obligatorios'
        });
    } else {
        var datos = new FormData();
        datos.append('usuario', usuario);
        datos.append('password', password);
        datos.append('accion', tipo);

        // AJAX
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'includes/models/modelo-admin.php', true);
        xhr.onload = function() {
            if(this.status === 200) {
                var respuesta = JSON.parse(xhr.responseText);
                if(respuesta.respuesta === 'ok') {
                    if(respuesta.tipo === 'crear') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Usuario creado',
                            text: 'El usuario se creó correctamente'
                        });
                    } else if (respuesta.tipo === 'login') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Login correcto',
                            text: 'Presiona en OK para ir a inicio'
                        })
                        .then(resultado => {
                            if(resultado.value) {
                                window.location.href = 'index.php';
                            }
                        });
                    }
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Ocurrió un error'
                    });
                }
                console.log(respuesta);
            }
        }
        xhr.send(datos);
    }
}