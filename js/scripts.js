var listaProyectos = document.querySelector('ul#proyectos');

eventListeners();

function eventListeners() {
    if(document.querySelector('.crear-proyecto a')) {
        document.querySelector('.crear-proyecto a').addEventListener('click', crearProyecto);
    }
    if(document.querySelector('.nueva-tarea')) {
        document.querySelector('.nueva-tarea').addEventListener('click', añadirTarea);
    }
    if(document.querySelector('.listado-pendientes')) {
        document.querySelector('.listado-pendientes').addEventListener('click', accionesTareas);
    }
  
    document.addEventListener('DOMContentLoaded', function() {
        actualizarProgreso();
    });
}

function crearProyecto(e) {
    e.preventDefault();

    var nuevoProyecto = document.createElement('li');
    nuevoProyecto.innerHTML = '<input type="text" id="nuevo-proyecto">';
    listaProyectos.appendChild(nuevoProyecto);

    var inputNuevoProyecto = document.querySelector('#nuevo-proyecto');
    inputNuevoProyecto.addEventListener('keypress', function(e) {
        var key = e.key;
        if(key === 'Enter') {
            guardarProyecto(inputNuevoProyecto.value);
            listaProyectos.removeChild(nuevoProyecto);
        }
    });
}

function guardarProyecto(proyecto) {
    var datos = new FormData();
    var xhr = new XMLHttpRequest();

    datos.append('proyecto', proyecto);
    datos.append('accion', 'crear');

    xhr.open('POST', 'includes/models/modelo-proyecto.php', true);
    xhr.onload = function () {
        if(this.status === 200) {
            var respuesta = JSON.parse(xhr.responseText);

            if(respuesta.respuesta === 'ok') {
                if(respuesta.accion === 'crear') {
                    // Crear proyecto
                    var nuevoProyecto = document.createElement('li');
                    nuevoProyecto.innerHTML = `
                        <a href="index.php?id=${respuesta.id}" id="proyecto: ${respuesta.id}">
                            ${respuesta.nombre}
                        </a>
                    `;
                    listaProyectos.appendChild(nuevoProyecto);
                    Swal.fire({
                        icon: 'success',
                        title: 'Proyecto creado',
                        text: 'El proyecto: ' + respuesta.nombre + ' se creó correctamente'
                    })
                    .then(resultado => {
                        if(resultado.value) {
                            window.location.href = 'index.php?id_proyecto=' + respuesta.id;
                        }
                    });
                } else {
                    // Actualizar o eliminar proyecto

                }
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Ocurrió un error'
                });
            }
            
            console.log(respuesta);
        }
    }
    xhr.send(datos);
}

function añadirTarea(e) {
    e.preventDefault();
    var nombreTarea = document.querySelector('.nombre-tarea').value;
    if(!nombreTarea) {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'La tarea no puede estar vacía'
        });
    } else {
        var datos = new FormData();
        var xhr = new XMLHttpRequest();

        datos.append('tarea', nombreTarea);
        datos.append('accion', 'crear');
        datos.append('id_proyecto', document.querySelector('#id_proyecto').value);

        xhr.open('POST', 'includes/models/modelo-tarea.php', true);
        xhr.onload = function () {
            if(this.status === 200) {
                var respuesta = JSON.parse(xhr.responseText);
    
                if(respuesta.respuesta === 'ok') {
                    if(respuesta.accion === 'crear') {
                        // Crear tarea
                        Swal.fire({
                            icon: 'success',
                            title: 'Tarea creada',
                            text: 'La tarea: ' + respuesta.tarea + ' se creó correctamente'
                        })
                        .then(resultado => {
                            if(resultado.value) {
                                var parrafoListaVacia = document.querySelectorAll('.lista-vacia');
                                if (parrafoListaVacia.length > 0) {
                                    document.querySelector('.lista-vacia').remove();
                                }
                                var nuevaTarea = document.createElement('li');
                                var listado = document.querySelector('.listado-pendientes ul')
                                nuevaTarea.id = 'tarea: ' + respuesta.id_tarea;
                                nuevaTarea.classList.add('tarea');
                                nuevaTarea.innerHTML = `
                                    <p>${respuesta.tarea}</p>
                                    <div class="acciones">
                                        <i class="far fa-check-circle"></i>
                                        <i class="fas fa-trash"></i>
                                    </div>
                                `;
                                listado.appendChild(nuevaTarea);
                                document.querySelector('.agregar-tarea').reset();
                                actualizarProgreso();
                            }
                        })
                    }
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Ocurrió un error'
                    });
                }
                console.log(respuesta);
            }
        }
        xhr.send(datos);
    }
}

function accionesTareas(e) {
    e.preventDefault();

    if(e.target.classList.contains('fa-check-circle')) {
        if(e.target.classList.contains('completo')) {
            e.target.classList.remove('completo');
            cambiarEstadoTarea(e.target, 0);
        } else {
            e.target.classList.add('completo');
            cambiarEstadoTarea(e.target, 1);
        }
    } else if(e.target.classList.contains('fa-trash')) {
        Swal.fire({
            title: '¿Estás seguro/a?',
            text: "Esta acción es permanente",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.isConfirmed) {
                var tarea = e.target.parentElement.parentElement;
                tarea.remove();
                eliminarTarea(tarea);
            }
          })
    }
}

function cambiarEstadoTarea(tarea, estado) {
    var idTarea = tarea.parentElement.parentElement.id.split(': ')[1];
    var datos = new FormData();
    var xhr = new XMLHttpRequest();

    datos.append('id_tarea', idTarea);
    datos.append('accion', 'actualizar');
    datos.append('estado', estado);

    xhr.open('POST', 'includes/models/modelo-tarea.php', true);
    xhr.onload = function () {
        if(this.status === 200) {
            var respuesta = JSON.parse(xhr.responseText);
            actualizarProgreso();
        }
        console.log(respuesta);
    }
    xhr.send(datos);
}

function eliminarTarea(tarea) {
    var idTarea = tarea.id.split(': ')[1];
    var datos = new FormData();
    var xhr = new XMLHttpRequest();

    datos.append('id_tarea', idTarea);
    datos.append('accion', 'eliminar');

    xhr.open('POST', 'includes/models/modelo-tarea.php', true);
    xhr.onload = function () {
        if(this.status === 200) {
            var respuesta = JSON.parse(xhr.responseText);
            Swal.fire(
                '¡Eliminado!',
                'La tarea fue eliminada',
                'success'
            )
            var listaTareasRestantes = document.querySelectorAll('li.tarea');
            if(listaTareasRestantes.length === 0) {
                document.querySelector('.listado-pendientes ul').innerHTML = "<p class='lista-vacia'>No hay tareas en este proyecto</p>";
            }
            actualizarProgreso();
        }
        console.log(respuesta);
    }
    xhr.send(datos);
}

function actualizarProgreso() {
    var tareas = document.querySelectorAll('li.tarea');
    var tareasCompletadas = document.querySelectorAll('i.completo');
  
    if(tareas && tareasCompletadas && tareasCompletadas.length === 0) {
        var porcentaje = document.querySelector('#porcentaje');
        porcentaje.style.width = '0%';
    }
    else if(tareas && tareasCompletadas && tareas.length > 0 && tareasCompletadas.length > 0) {
        var avance = Math.round((tareasCompletadas.length/tareas.length)*100);
        var porcentaje = document.querySelector('#porcentaje');
        porcentaje.style.width = avance+'%';

        if(avance === 100) {
            Swal.fire({
                icon: 'success',
                title: '¡Proyecto terminado!',
                text: 'Ya no tienes tareas pendientes'
            });
        }
    }
}